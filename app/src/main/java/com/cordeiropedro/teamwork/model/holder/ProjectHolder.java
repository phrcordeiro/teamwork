package com.cordeiropedro.teamwork.model.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cordeiropedro.teamwork.R;

import butterknife.BindView;

public class ProjectHolder extends AbstractRecyclerViewHolder {

    @BindView(R.id.project_name)
    public TextView projectName;
    @BindView(R.id.project_create_date)
    public TextView projectCreateDate;
    @BindView(R.id.company_name)
    public TextView companyName;
    @BindView(R.id.project_logo)
    public ImageView projectLogo;

    public ProjectHolder(View itemView) {
        super(itemView);
    }
}
