package com.cordeiropedro.teamwork.model;

import com.google.gson.FieldNamingStrategy;

import java.lang.reflect.Field;

public class TWFieldNamingStrategy implements FieldNamingStrategy {

    @Override
    public String translateName(Field field) {
        String name = field.getName();
        String nameAfter = name.replaceAll("-", "");
        return nameAfter;
    }
}
