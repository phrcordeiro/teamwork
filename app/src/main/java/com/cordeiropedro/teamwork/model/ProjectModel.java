package com.cordeiropedro.teamwork.model;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProjectModel {

    public boolean starred;
    public boolean replyByEmailEnabled;
    public boolean show_announcement;
    public boolean harvest_timers_enabled;
    public boolean filesAutoNewVersion;
    public boolean privacyEnabled;
    public boolean isProjectAdmin;
    public boolean notifyeveryone;
    public String status;
    public String subStatus;
    public String defaultPrivacy;
    public String logo;
    public String id;
    public String name;
    public String description;
    public String announcement;
    @SerializedName("start-page")
    public String startpage;
    public String announcementHTML;
    @SerializedName("created-on")
    public Date createdon;
    @SerializedName("last-changed-on")
    public Date lastchangedon;
    public Category category;
    public Company company;
    public Defaults defaults;
    public List<String> tags;

    public ProjectModel() {

    }

    public static ArrayList<ProjectModel> getProjectList(JSONArray jsonArray) {

        Type listType = new TypeToken<ArrayList<ProjectModel>>() {
        }.getType();

        return new GsonBuilder()
                .setFieldNamingStrategy(new TWFieldNamingStrategy())
                .create()
                .fromJson(String.valueOf(jsonArray), listType);

    }
}
