package com.cordeiropedro.teamwork.controller.connection.listener;

import cz.msebera.android.httpclient.Header;


public interface IListeners {

    void onNetworkError(int statusCode,
                        Header[] headers,
                        byte[] responseBody,
                        Throwable error);

}
