package com.cordeiropedro.teamwork.controller.connection.listener;

import com.cordeiropedro.teamwork.model.ProjectModel;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public abstract class ProjectListener implements IListeners {

    public abstract void onSucess(ArrayList<ProjectModel> content, Header[] headers);
}
