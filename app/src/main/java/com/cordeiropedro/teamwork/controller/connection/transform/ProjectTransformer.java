package com.cordeiropedro.teamwork.controller.connection.transform;

import com.cordeiropedro.teamwork.controller.connection.listener.ProjectListener;
import com.cordeiropedro.teamwork.model.ProjectModel;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ProjectTransformer extends AbstractTransformer<ProjectListener> {

    public ProjectTransformer(ProjectListener listener) {
        super(listener);
    }

    @Override
    public void transform(JSONObject jsonObject, int statusCode, Header[] headers) {
        if (jsonObject.optString("STATUS").equals("OK")) {
            ArrayList<ProjectModel> projectModel =
                    ProjectModel.getProjectList(jsonObject.optJSONArray("projects"));
            listener.onSucess(projectModel, headers);
        }
    }
}
