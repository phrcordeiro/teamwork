package com.cordeiropedro.teamwork.controller.connection.requester;

import android.content.Context;

import com.cordeiropedro.teamwork.controller.connection.URLs;
import com.cordeiropedro.teamwork.controller.connection.transform.ProjectTransformer;

public class ProjectRequester extends AbstractRequester {

    public ProjectRequester(Context context) {
        super(context);
    }

    public void getProjects(ProjectTransformer transformer) {
        controller.get(context, URLs.URL_PROJECTS, transformer);
    }

}
