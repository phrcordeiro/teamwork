package com.cordeiropedro.teamwork.controller.connection.transform;

import com.cordeiropedro.teamwork.controller.connection.listener.IListeners;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;


public abstract class AbstractTransformer<T extends IListeners>
        extends AsyncHttpResponseHandler {

    protected T listener;

    public AbstractTransformer(T listener) {
        this.listener = listener;
    }

    //----------------------------------------------------------------------------------------------
    // Overrides
    //----------------------------------------------------------------------------------------------

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        if (statusCode >= 300) {
            listener.onNetworkError(statusCode, headers, responseBody, error);
        } else {
            onSuccess(statusCode, headers, responseBody);
        }
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        transform(statusCode, headers, responseBody);
    }

    //----------------------------------------------------------------------------------------------
    // Custom Methods
    //----------------------------------------------------------------------------------------------`

    public void transform(int statusCode, Header[] headers, byte[] responseBody) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(new String(responseBody));
        } catch (JSONException e) {
            listener.onNetworkError(statusCode, headers, responseBody, e);
            return;
        } catch (NullPointerException e) {
            if (statusCode < 300 && statusCode >= 200) {
                transform(jsonObject, statusCode, headers);
                return;
            }
            listener.onNetworkError(statusCode, headers, responseBody, e);
            return;
        }
        transform(jsonObject, statusCode, headers);
    }

    public abstract void transform(JSONObject jsonObject, int statusCode, Header[] headers);

}
