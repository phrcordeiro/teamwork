package com.cordeiropedro.teamwork.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.cordeiropedro.teamwork.R;
import com.cordeiropedro.teamwork.controller.connection.listener.ProjectListener;
import com.cordeiropedro.teamwork.controller.connection.requester.ProjectRequester;
import com.cordeiropedro.teamwork.controller.connection.transform.ProjectTransformer;
import com.cordeiropedro.teamwork.model.ProjectModel;
import com.cordeiropedro.teamwork.view.adpter.ProjectAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.project_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.linear_layout)
    LinearLayout linearLayout;

    ProjectRequester projectRequester;
    ProjectAdapter adapter;
    RecyclerView.LayoutManager mLayoutManager;
    List<ProjectModel> projectModels;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        projectRequester = new ProjectRequester(this);
        projectListSetup();
        setSwipeRefresh();

    }

    @Override
    protected void onResume() {
        super.onResume();
        progressON();
        updateList();

    }

    private void setSwipeRefresh() {
        swipeRefreshLayout.setColorSchemeResources(R.color.blue);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateList();
            }
        });
    }

    private void projectListSetup() {
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        projectModels = new ArrayList<>();
        adapter = new ProjectAdapter(projectModels, this);
        mRecyclerView.setAdapter(adapter);
    }

    private void updateList() {
        projectRequester.getProjects(new ProjectTransformer(new ProjectListener() {
            @Override
            public void onSucess(ArrayList<ProjectModel> content, Header[] headers) {
                fillProjectList(content);
                progressOFF();
            }

            @Override
            public void onNetworkError(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                notification(String.valueOf(statusCode));
                progressOFF();
            }
        }));
    }

    public void notification(String msg) {
        Snackbar.make(linearLayout, getString(R.string.server_error) + msg, Snackbar.LENGTH_LONG).show();
    }


    public void fillProjectList(List<ProjectModel> content) {
        projectModels.clear();
        projectModels.addAll(content);
        adapter.notifyDataSetChanged();
    }

    private void progressON() {
        progressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }

    private void progressOFF() {
        progressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }
}
