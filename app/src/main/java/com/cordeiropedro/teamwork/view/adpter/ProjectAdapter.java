package com.cordeiropedro.teamwork.view.adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cordeiropedro.teamwork.R;
import com.cordeiropedro.teamwork.controller.connection.requester.ProjectRequester;
import com.cordeiropedro.teamwork.model.DateUtility;
import com.cordeiropedro.teamwork.model.ProjectModel;
import com.cordeiropedro.teamwork.model.holder.ProjectHolder;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class ProjectAdapter extends FasterCardAdapter<ProjectModel> {

    ProjectRequester projectRequester;

    public ProjectAdapter(List<ProjectModel> listObjects, Context context) {
        super(listObjects, context);
    }

    @Override
    protected int layoutResourceForItem(int position) {
        return R.layout.card_project;
    }

    @Override
    protected void fillHolder(RecyclerView.ViewHolder abstractHolder, int position) {
        projectRequester = new ProjectRequester(getContext());
        ProjectHolder holder = (ProjectHolder) abstractHolder;
        ProjectModel model = listObjects.get(position);
        holder.projectName.setText(model.name);
        holder.companyName.setText(model.company.name);
        holder.projectCreateDate.setText(DateUtility.dateForString(model.createdon));
        ImageLoader.getInstance().displayImage(model.logo, holder.projectLogo);
    }

    @Override
    protected RecyclerView.ViewHolder inflateView(View view, int position) {
        return new ProjectHolder(view);
    }

}
